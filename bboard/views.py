from django.http import HttpResponse

from django.shortcuts import render


# Create your views here.
from bboard.models import Bb


def index(request):
    s = 'Список объявлений \r\n\r\n'
    for i in Bb.objects.order_by('-published'):
        s += i.title + '\r\n' + i.content + '\r\n\r\n'
    return HttpResponse(s, content_type='text/p;ain; charset= utf-8')
